provider "aws" {
  region = local.region
  access_key = ""
  secret_key = ""

  default_tags {
    tags = {
      Project = "terraform-aws-autoscaling"
    }
  }

  # Make it faster by skipping something
  skip_get_ec2_platforms      = true
  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_credentials_validation = true
  skip_requesting_account_id  = true
}

locals {
  name   = "example"
  region = "us-east-1"

  tags = [
    {
      key                 = "Project"
      value               = "megasecret"
      propagate_at_launch = true
    },
    {
      key                 = "foo"
      value               = "something"
      propagate_at_launch = true
    },
  ]

  tags_as_map = {
    Owner       = "user"
    Environment = "dev"
  }

}

################################################################################
# Supporting Resources
################################################################################

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.0"

  name = local.name
  cidr = "10.99.0.0/18"

  azs             = ["${local.region}a"]
  public_subnets  = ["10.99.0.0/24"]
  private_subnets = ["10.99.3.0/24"]

  enable_nat_gateway = true
  single_nat_gateway = true

  tags = local.tags_as_map
}

module "asg_sg" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.0"

  name        = local.name
  description = "A security group"
  vpc_id      = module.vpc.vpc_id

  egress_rules = ["all-all"]

  tags = local.tags_as_map
}

data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name = "name"

    values = [
      "amzn-ami-hvm-*-x86_64-gp2",
    ]
  }
}

resource "aws_iam_service_linked_role" "autoscaling" {
  aws_service_name = "autoscaling.amazonaws.com"
  description      = "A service linked role for autoscaling"
  custom_suffix    = local.name

  # Sometimes good sleep is required to have some IAM resources created before they can be used
  provisioner "local-exec" {
    command = "start /wait timeout 10"
  }
}

resource "aws_iam_instance_profile" "ssm" {
  name = "complete-${local.name}"
  role = aws_iam_role.ssm.name
  tags = local.tags_as_map
}

resource "aws_iam_role" "ssm" {
  name = "complete-${local.name}"
  tags = local.tags_as_map

  assume_role_policy = <<-EOT
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "sts:AssumeRole",
        "Principal": {
          "Service": "ec2.amazonaws.com"
        },
        "Effect": "Allow",
        "Sid": ""
      }
    ]
  }
  EOT
}

################################################################################
# Default
################################################################################

# Launch template
module "default_lt" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 4.0"

  # Autoscaling group
  name = "default-lt-${local.name}"

  vpc_zone_identifier = module.vpc.private_subnets
  min_size            = 2
  max_size            = 5
  desired_capacity    = 2

  # Launch template
  use_lt    = true
  create_lt = true

  image_id      = data.aws_ami.amazon_linux.id
  instance_type = "t2.medium"

  tags        = local.tags
  tags_as_map = local.tags_as_map
}

resource "aws_autoscaling_policy" "example" {
  name = "example"
  adjustment_type        = "PercentChangeInCapacity"
  policy_type            = "TargetTrackingScaling"  
  autoscaling_group_name = module.default_lt.autoscaling_group_name
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 45.0
  }
}


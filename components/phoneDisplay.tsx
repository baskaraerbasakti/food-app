import React from 'react'
import styles from '../styles/Phone.module.css'


const PhoneDisplay = () => {
    return (
        <div className={styles.boxPhone}>        
            <div className={styles.phoneOrange}>
                <img src="/phone-orange.png" alt="phone-orange" loading="lazy" width="780px" height="650px"/>
            </div>
            <div className={styles.phoneWhite}>
                <img src="/phone-white.png" alt="phone-white" loading="lazy" width="780px" height="650px"/>
            </div>
            
        </div>
    )
}

export default PhoneDisplay
